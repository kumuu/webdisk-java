/*
 Navicat Premium Data Transfer

 Source Server         : WebDisk
 Source Server Type    : MySQL
 Source Server Version : 50562
 Source Host           : 121.199.52.206:3306
 Source Schema         : WebDisk

 Target Server Type    : MySQL
 Target Server Version : 50562
 File Encoding         : 65001

 Date: 02/08/2020 15:45:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for levels
-- ----------------------------
DROP TABLE IF EXISTS `levels`;
CREATE TABLE `levels` (
  `level` int(10) NOT NULL COMMENT '等级',
  `size` int(11) NOT NULL COMMENT '容量',
  PRIMARY KEY (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of levels
-- ----------------------------
BEGIN;
INSERT INTO `levels` VALUES (1, 20);
INSERT INTO `levels` VALUES (2, 200);
INSERT INTO `levels` VALUES (3, 2048);
INSERT INTO `levels` VALUES (9, 20000);
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL COMMENT '用户 ID',
  `username` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户名称',
  `password` varchar(36) COLLATE utf8mb4_bin NOT NULL COMMENT '用户密码',
  `level` int(1) unsigned zerofill DEFAULT '1' COMMENT '用户权限等级',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('00943b517d0d1486b8a70c75741ad4d3', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-07-04 18:05:27', '2020-07-15 09:51:49');
INSERT INTO `user` VALUES ('19ada5cf2bd9a0c3f81c168e27a31710', '老八', '5563d58b6c21b7a2b1b95bf45d4f2db9', 1, '2020-07-18 17:53:31', '2020-07-18 17:53:31');
INSERT INTO `user` VALUES ('65a06ee5e21e9b303a35347b7f6cfb93', 'yy', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-07-04 18:05:27', '2020-07-04 18:05:27');
INSERT INTO `user` VALUES ('83aff4ae731dc8bee81169e241c4e635', 'kum', 'e10adc3949ba59abbe56e057f20f883e', 9, '2020-07-04 18:05:27', '2020-07-04 18:05:27');
INSERT INTO `user` VALUES ('98420bf45e2876a0254a8db4bec5b677', 'testtt', 'e10adc3949ba59abbe56e057f20f883e', 9, '2020-07-15 09:41:57', '2020-07-15 09:50:24');
INSERT INTO `user` VALUES ('a0d706921e9c6ad2c94a39d44c9a2dd9', 'user', 'e10adc3949ba59abbe56e057f20f883e', 5, '2020-07-11 17:31:27', '2020-07-15 09:49:31');
INSERT INTO `user` VALUES ('cdd5193e6defb94ee73a26224c6f01c8', 'uuuu', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-07-15 09:48:00', '2020-07-15 09:48:00');
INSERT INTO `user` VALUES ('ce24479270b502d9ddf96ab3282aceba', 'hhh', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-07-12 10:05:09', '2020-07-12 10:05:09');
INSERT INTO `user` VALUES ('e0b661931d1ad4cc4511330285ce5658', 'test', 'e10adc3949ba59abbe56e057f20f883e', 2, '2020-07-11 17:19:36', '2020-07-11 19:17:08');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
