package com.kum.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Share {
    private String id;
    private String shareName;
    private Date shareDate;
    private boolean isEncrypt;


}
