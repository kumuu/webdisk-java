package com.kum.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Share_me extends Share{
    @Nullable
    private String pwd;
    private String filePath;

    public Share_me(String uuid, String pwd, String filePath, Date shareData) {
        setId(uuid);
        setShareDate(shareData);
        this.pwd = pwd;
        this.filePath = filePath;

    }
}
