package com.kum.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class down_ret extends BaseMsg{
    private String fileUrl;
    private String fileName;

}
