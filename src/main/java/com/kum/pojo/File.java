package com.kum.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class File {
    private String name;
    @Nullable
    private Long size;
    @Nullable
    private Date lastModified;

}
