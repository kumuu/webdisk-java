package com.kum.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Share_ret extends BaseMsg{
    private String number;
    @Nullable
    private String pwd;

}
