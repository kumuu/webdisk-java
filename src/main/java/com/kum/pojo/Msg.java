package com.kum.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Msg extends BaseMsg{
    private int code;
    private String data;
    @Nullable
    private String token;

}
