package com.kum.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User_info extends Msg{
    private long spaceSize;
    private long levelSpace;


}
