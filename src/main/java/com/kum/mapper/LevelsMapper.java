package com.kum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kum.pojo.Levels;
import org.springframework.stereotype.Component;

@Component
public interface LevelsMapper extends BaseMapper<Levels> {
}
