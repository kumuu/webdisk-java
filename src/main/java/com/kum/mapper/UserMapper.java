package com.kum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kum.pojo.User;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
@Component
public interface UserMapper extends BaseMapper<User> {

    void deleteByMap(HashMap<String,String> map);
}
