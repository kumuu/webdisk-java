package com.kum.controller;


import com.alibaba.fastjson.JSONObject;
import com.kum.mapper.LevelsMapper;
import com.kum.pojo.Levels;
import com.kum.utils.JwtUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@RestController
@RequestMapping("/api")
public class ContentController {
    @Resource
    private LevelsMapper levelsMapper;


    /*
       获得对应等级的空间容量.
    */

    @GetMapping("/getCapacity")
    private String getCapacity(HttpServletRequest request) {
        HashMap<String, Object> map = new HashMap<>();

        JSONObject payload = JwtUtil.getPayload(request.getHeader("token"));
        map.put("level", payload.get("level"));

        return levelsMapper.selectByMap(map).get(0).getSize();

    }


}
