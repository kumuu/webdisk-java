package com.kum.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kum.annotations.TokenVerify;
import com.kum.annotations.UserId;
import com.kum.mapper.UserMapper;
import com.kum.pojo.Msg;
import com.kum.pojo.User;
import com.kum.pojo.User_info;
import com.kum.service.UserService;
import com.kum.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @Resource
    private UserMapper userMapper;
    @Autowired
    private UserService userService;

    @GetMapping("/getUserInfo")
    @TokenVerify
    private String getUserInfo(@UserId String userId) {
        long spaceSize = userService.getSpaceSize(userId);
        int level = UserService.getUserLevel(userId);
        long levelSpace = UserService.getLevelSpace(level);
        return new Msg(0, new User_info(spaceSize, levelSpace).toJson(), null).toJson();


    }


    @PostMapping("/upDataLevel")
    private String upDataLevel(@RequestBody JSONObject jsonObject) {
        String target = jsonObject.get("username").toString();
        String level = jsonObject.get("level").toString();

        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", target);
        User user = new User();
        user.setLevel(Integer.parseInt(level));

        int update = userMapper.update(user, wrapper);
        if (update == 0) {
            return new Msg(1, "修改失败!", null).toJson();
        }
        return new Msg(0, "修改成功!", null).toJson();


    }

    @PostMapping("/upDataPwd")
    private String upDataPwd(@RequestBody JSONObject jsonObject, HttpServletRequest req) {
        String username = JwtUtil.getPayload(req.getHeader("token")).get("username").toString();
        String password = jsonObject.get("password").toString();
        String newPassword = jsonObject.get("newPassword").toString();
        HashMap<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put("password", DigestUtils.md5DigestAsHex(password.getBytes()));
        System.out.println(username);
        List<User> users = userMapper.selectByMap(map);
        if (users.size() == 0) {
            return new Msg(1, "原密码错误,请检查！", null).toJson();

        }
        User user = users.get(0);
        user.setPassword(DigestUtils.md5DigestAsHex(newPassword.getBytes()));
        
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("id", user.getId());
        int i = userMapper.update(user, wrapper);
        if (i > 0) {
            return new Msg(0, "密码修改成功!", null).toJson();
        }
        return new Msg(2, "未知错误,请重试!", null).toJson();


    }

    @PostMapping("/selectUser")
    private String selectUser(@RequestBody JSONObject jsonObject) {
        String username = jsonObject.get("username").toString();
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.like("username", username);
        List<User> users = userMapper.selectList(wrapper);
        return JSONObject.toJSONString(users);
    }

    @GetMapping("/selectUserAllList")
    private String selectUserAllList() {
        List<User> userList = userMapper.selectList(null);
        return JSON.toJSONString(userList);
    }

    @PostMapping("/delUser")
    private String delUser(@RequestBody JSONObject jsonObject) {
        String username = jsonObject.get("username").toString();
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        userMapper.delete(wrapper);
        return new Msg(0, "删除成功！", null).toJson();

    }


}
