package com.kum.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.kum.annotations.TokenVerify;
import com.kum.mapper.UserMapper;
import com.kum.pojo.Msg;
import com.kum.pojo.User;
import com.kum.service.oss.InitSpace;
import com.kum.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;


@RestController
@RequestMapping("/api")
public class LoginController {

    @Resource
    private UserMapper userMapper;
    @Autowired
    private InitSpace initSpace;

    @PostMapping("/login")
    public String login(@RequestBody JSONObject jsonObject, HttpServletRequest request) {


        String username = jsonObject.get("username").toString();
        String password = jsonObject.get("password").toString();

        HashMap<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put("password", DigestUtils.md5DigestAsHex(password.getBytes()));
        List<User> list = userMapper.selectByMap(map);
        if (list.size() == 0) {
            return new Msg(1, "用户名或密码错误!", null).toJson();
        }
        User user = list.get(0);

        /**
         *   如果当前访问的是管理员登录的地址,那么就需要在 header > AdminLogin 属性为 true 并且权限为 9(管理员).
         */
        String header = request.getHeader("AdminLogin");
        if (header != null && header.equals("true") && user.getLevel() != 9) {
            return new Msg(2, "你没有管理员的权限!", null).toJson();
        }

        String token = JwtUtil.sign(user.getUsername(), user.getId(), user.getLevel() + "");

        JSONObject parseObject = JSON.parseObject(new Msg(0, "登录成功!", token).toJson());
        parseObject.put("userId", user.getId());
        parseObject.put("level", user.getLevel());

        return parseObject.toJSONString();

    }

    @PostMapping("/register")
    public String register(@RequestBody JSONObject jsonObject) {
        //
        String username = jsonObject.get("username").toString();
        String password = jsonObject.get("password").toString();
        HashMap<String, Object> map = new HashMap<>();
        map.put("username", username);
        if (userMapper.selectByMap(map).size() != 0) {
            return new Msg(1, "用户名重复!", null).toJson();
        }
        User user = new User(username, DigestUtils.md5DigestAsHex(password.getBytes()), 1);

        if (userMapper.insert(user) == 1) {

            String userID = userMapper.selectByMap(map).get(0).getId();

            //以 UserName 为命名在 user_space 总创建目录.
            initSpace.initSpace(userID);
            return new Msg(0, "注册成功!", JwtUtil.sign(username, userID ,"1")).toJson();

        }
        return new Msg(2, "未知错误!", null).toJson();

    }

    /*
      鉴定 token 是否伪造.
     */
    @GetMapping("/getTokenVerity")
    public int getTokenVerity(@RequestParam("token") String token) {
        int res = JwtUtil.verity(token);
        if (res == 0 && JwtUtil.getPayload(token).get("userLevel").toString().equals("9")) {
            res = 9;
        }
        return res;

    }

    @TokenVerify
    @GetMapping("test")
    public int test() {

        return 1;
    }

}


