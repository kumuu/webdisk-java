package com.kum.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.model.OSSObjectSummary;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kum.annotations.TokenVerify;
import com.kum.annotations.UserId;
import com.kum.mapper.UserMapper;
import com.kum.pojo.File;
import com.kum.pojo.Msg;
import com.kum.pojo.User;
import com.kum.service.UserService;
import com.kum.service.oss.OSSClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api/oss")
public class OSSController {
    private final OSSClient ossClient;
    @Resource
    private UserService userService;
    @Resource
    private UserMapper userMapper;

    @Autowired
    public OSSController(OSSClient ossClient) {
        this.ossClient = ossClient;
    }

    @RequestMapping("/upload")
    @TokenVerify
    private String upload(@RequestParam("path") String path,
                          @RequestParam("file") MultipartFile file, @UserId String userId) throws IOException {
        long size = userService.getSurplusSpace(userId);
        if (size <= 0 || size - file.getSize() <= 0) {
            return new Msg(1, "空间不足，请提升您的权限！", null).toJson();
        }
        path = userId + path + file.getOriginalFilename();
        ossClient.putFile(path, file.getBytes());
        return new Msg(0, "上传完毕", null).toJson();

    }

    @PostMapping("/getFileList")
    @TokenVerify
    private String getFileList(@RequestBody JSONObject jsonObject, @UserId String userId) {
        if (userId.equals("0")) {
            String name = jsonObject.get("username").toString();
            QueryWrapper<User> wrapper = new QueryWrapper<>();
            wrapper.eq("username", name);
            userId = userMapper.selectList(wrapper).get(0).getId();
        }
        String path = userId + jsonObject.get("path").toString();
        return ossClient.getFileList(path, false);

    }

    @PostMapping("/getDirList")
    @TokenVerify
    private String getDirList(@RequestBody JSONObject jsonObject, @UserId String userId) {
        String path = userId + jsonObject.get("path").toString();
        return ossClient.getFileList(path, true);

    }

    @PostMapping("/deleteFile")
    @TokenVerify
    private String deleteFile(@RequestBody JSONObject jsonObject, @UserId String userId) {
        JSONArray files = jsonObject.getJSONArray("files");
        List<String> list = files.toJavaList(String.class);
        if (userId.equals("0")) {
            userId = jsonObject.get("userId").toString();
        }
        ossClient.deleteFile(list, userId);
        return new Msg(0, "删除成功", null).toJson();

    }

    @PostMapping("/deleteFile_")
    private String deleteFile_(@RequestBody JSONObject jsonObject){
        String path = jsonObject.get("path").toString();
        ossClient.deleteFile_(path);
        return new Msg(0,"删除成功！",null).toJson();
    }


    @PostMapping("/reNameFile")
    @TokenVerify
    private String reNameFile(@RequestBody JSONObject jsonObject, @UserId String userId) {
        String path = jsonObject.get("path").toString();
        String name = jsonObject.get("name").toString();
        String newName = jsonObject.get("newName").toString();
        ossClient.reNameFile(userId, path, name, newName);
        return new Msg(0, "重命名成功", null).toJson();

    }

    @PostMapping("/mkDir")
    @TokenVerify
    private String mkDir(@RequestBody JSONObject jsonObject, @UserId String userId) {
        String path = userId + jsonObject.get("path").toString();
        ossClient.mkDir(path);
        return new Msg(0, "成功建立文件夹", null).toJson();

    }


    @PostMapping("/moveFile")
    @TokenVerify
    private String copyFiles(@RequestBody JSONObject jsonObject, @UserId String userId) {
        JSONArray files = jsonObject.getJSONArray("files");
        String targetPath = userId + jsonObject.get("target").toString();
        List<String> list = files.toJavaList(String.class);

        ossClient.moveFile(list, targetPath, userId);
        return new Msg(0, "移动成功", null).toJson();

    }


    @PostMapping("/selectFileList")
    @TokenVerify
    private String selectFileList(@RequestBody JSONObject jsonObject, @UserId String userId) {
        String name = jsonObject.get("name").toString();
        return ossClient.selectFile(userId, name);
    }

    @PostMapping("/getFileFormatList")
    @TokenVerify
    private String getFileFormatList(@RequestBody JSONObject jsonObject, @UserId String userId) {
        String format = jsonObject.get("format").toString();
        return ossClient.getFileFormatList(userId, format);

    }

    @PostMapping("downloadFile")
    private String downloadFile(@RequestBody JSONObject jsonObject, @UserId String userId) throws IOException {
        String path = jsonObject.get("path").toString();
        return new Msg(0, ossClient.downloadFile(userId, path), null).toJson();

    }

    @GetMapping("/getAllFile")
    private String getAllFile(@RequestParam String username) {
        String userId = userService.getUserId(username);
        List<OSSObjectSummary> allFile = ossClient.getAllFileList(userId);
        ArrayList<File> list = new ArrayList<>();
        for (OSSObjectSummary sum :
                allFile) {
            if (sum.getSize() == 0) continue;
            String key = sum.getKey();
            String name = key.substring(11);
            if (name.length() < 1) continue;
            list.add(new File(name, sum.getSize(), sum.getLastModified()));

        }

        return JSON.toJSONString(list);


    }


}
