package com.kum.Interceptor;

import com.kum.annotations.UserId;
import com.kum.utils.JwtUtil;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class InfoResolver implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(UserId.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
        String admin = webRequest.getHeader("AdminLogin");
        if (admin != null && admin.equals("true")) {
           //如果是管理员的话，那么就需要自行提供删除那个用户~
            return "0";
        }
        return JwtUtil.getPayload(webRequest.getHeader("token")).get("userId").toString();

    }
}
