package com.kum.service;

import com.alibaba.fastjson.JSON;
import com.kum.pojo.Msg;
import com.kum.pojo.Share;
import com.kum.pojo.Share_me;
import com.kum.pojo.Share_ret;
import com.kum.service.oss.OSSClient;
import com.kum.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class ShareService {
    private static RedisTemplate<String, Object> redisTemplate;
    private static RedisUtil redisUtil;

    @Autowired
    public ShareService(RedisTemplate<String, Object> redisTemplate, RedisUtil redisUtil) {
        this.redisTemplate = redisTemplate;
        this.redisUtil = redisUtil;
    }


    /**
     * 向 Redis 中加入分享
     *
     * @param shareName 分享人名称。
     * @param shareData 分享时间
     * @param filePath  被分享文件的路径
     * @param isEncrypt 是否加密
     */
    public String addShare(String shareName, Date shareData, String filePath, boolean isEncrypt) {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        String fileKey = "Path:" + uuid;
        String ShareKey = "Share:" + uuid;
        String meShareKey = "Name:" + shareName + ":" + uuid;
        String pwd = null;
        if (isEncrypt) {
            pwd = RedisUtil.getStringRandom(4);
            fileKey += ":" + pwd;
        }
        //此时存储的是完整的地址
        filePath = OSSClient.basePath + filePath;
        putShare(fileKey, filePath);
        putShare(meShareKey,new Share_me(uuid,pwd,filePath,shareData));
        putShare(ShareKey, new Share(uuid, shareName, shareData, isEncrypt));

        return new Msg(0, new Share_ret(uuid, pwd).toJson(), null).toJson();

    }

    /**
     * 获得分享的文件
     * @param key 秘钥
     * @param pwd 提取码
     * @return
     */
    public String getShare(String key, String pwd) {
        key = "Path:" + key;
        if (!pwd.equals("0")) key += ":" + pwd;
        try {
            System.out.println(key);
            return redisTemplate.opsForValue().get(key).toString();
        } catch (NullPointerException e) {
            return "0";
        }

    }

    /**
     * 获得当前服务器所有分享列表
     * @return
     */
    public String getShareList() {
        return getPrefix("Share");
    }

    /**
     * 获得指定用户分享的文件列表
     * @param name 用户名
     * @return
     */
    public String getShareMeList(String name){
        return getPrefix("Name:" + name);
    }

    /**
     * 获得在 Redis 中指定前缀的列表，并将其转换为 Json
     * @param prefix
     * @return
     */
    public String getPrefix(String prefix) {
        Set<String> keys = redisTemplate.keys(prefix + "*");
        List<Object> list = redisTemplate.opsForValue().multiGet(keys);
        return JSON.toJSONString(list);
    }


    /**
     * 将分享加入 Redis 并设置过期时间为一天
     *
     * @param key
     * @param value
     */
    private static void putShare(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
        redisTemplate.expire(key, RedisUtil.DEFAULT_EXPIRE, TimeUnit.SECONDS);

    }


}
