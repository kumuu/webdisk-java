package com.kum.service.oss;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class InitSpace {

    private final OSSClient ossClient;

    @Autowired
    public InitSpace(OSSClient ossClient) {
        this.ossClient = ossClient;
    }

    /**
     * 当用户注册成功后创建用户空间
     * @param userId
     */
    public void initSpace(String userId){
        String text = "如果你看到此文件，代表你的空间已成功建立。";
        ossClient.putFile( userId + "/temp.txt",text.getBytes());
        ossClient.putFile( userId + "/回收站/temp.txt",text.getBytes());
        ossClient.putFile( userId + "/隐私空间/temp.txt",text.getBytes());

    }





}
