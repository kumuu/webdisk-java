package com.kum;

import com.kum.mapper.UserMapper;
import com.kum.pojo.Msg;
import com.kum.pojo.User;
import com.kum.service.ShareService;
import com.kum.service.UserService;
import com.kum.service.oss.OSSClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Random;

@SpringBootTest
class WebDiskApplicationTests {
//    @Resource
//    private UserMapper userMapper;
//    @Autowired
//    private OSSClient ossClient;
//    @Autowired
//    private RedisTemplate<String, Object> rt;
//    @Autowired
//    private ShareService shareService;

    @Autowired
    private UserService userService;


    @Test
    void redis_2() {
        System.out.println(userService.getSpaceSize("65a06ee5e21e9b303a35347b7f6cfb93"));
    }


    @Test
    void name() {
        System.out.println(DigestUtils.md5DigestAsHex("123456".getBytes()));

    }
}
